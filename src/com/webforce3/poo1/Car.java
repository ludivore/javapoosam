package com.webforce3.poo1;

public class Car {

	private String brand;
	private double initialPriceprice;
	private int manufacturingYear;
	private String registrationNumber;
	private final double TAUX = 0.02;
	User user;

	// le constructeur
	public Car(String brand, int initialPriceprice, int manufacturingYear, String registrationNumber, User user) {
		this.brand = brand;
		this.initialPriceprice = initialPriceprice;
		this.manufacturingYear = manufacturingYear;
		this.registrationNumber = registrationNumber;
		this.user = user;
	}

	//getter user
	public User getUser() {
		return user;
	}
	
	//setter user
	public void setUser(User user) {
		this.user = user;
	}

	// setters
	public void setBrand(String brand) {
		this.brand = brand;
	}

	public void setInitialPriceprice(int initialPriceprice) {
		this.initialPriceprice = initialPriceprice;
	}

	public void setManufacturingYear(int manufacturingYear) {
		this.manufacturingYear = manufacturingYear;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}
	

	// getters
	public String getBrand() {
		return this.brand;
	}

	public double getInitialPriceprice() {
		return this.initialPriceprice;
	}

	public int getManufacturingYear() {
		return this.manufacturingYear;
	}

	public String getRegistrationNumber() {
		return this.registrationNumber;
	}

	// methode calcul

	public void calculEstimation(int manufacturingYear, String brand, double initialPriceprice) {

		if (manufacturingYear >= 2010 && brand.equals("Mercedes")) {
			System.out.println(initialPriceprice = (initialPriceprice / 2));
		} else if (manufacturingYear <= 1980) {
			System.out.println(initialPriceprice = (initialPriceprice / 6));
		} else {
			System.out.println(initialPriceprice = (initialPriceprice / 4));
		}
	}

	public void calculAssurance(double initialPriceprice){
		System.out.println("Votre montant assurance : " + initialPriceprice * TAUX);	
	}
	
	
	
}
