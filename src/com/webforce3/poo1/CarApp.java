package com.webforce3.poo1;

public class CarApp {

public static void main(String[] args) {
		
		 
		//initialisation de 3 instances de la classe car
		Car car1 = new Car("Peugeot", 5000, 2000, "A1234", new User("Durand", "2 rue des ecrevisses", "durand@gmail.com", "password1"));
		Car car2 = new Car("Renault", 6000, 2010, "A1235", new User("Dupont", "3 rue des ecrevisses", "dupont@gmail.com", "password2"));
		Car car3 = new Car("Mercedes", 15000, 2020, "A1236", new User("Brodut", "4 rue des ecrevisses", "brodut@gmail.com", "password3"));
		
	
		System.out.println("-------------------------Voiture----------------------------------------");
		System.out.println("Marque " + car1.getBrand() + "\n Prix : " +  car1.getInitialPriceprice() + "\n Ann�e de production : " + car1.getManufacturingYear() + "\n Ref. : " + car1.getRegistrationNumber() + "\n Acqu�reur: " + car2.getUser().name);
		System.out.println("-------------------------Voiture----------------------------------------");
		System.out.println("Marque " + car2.getBrand() + "\n Prix : " +  car2.getInitialPriceprice() + "\n Ann�e de production : "  + car2.getManufacturingYear() + "\n Ref. : " + car2.getRegistrationNumber() + "\n Acqu�reur: " + car2.getUser().name);
		System.out.println("-------------------------Voiture----------------------------------------");
		System.out.println("Marque " + car3.getBrand() + "\n Prix : " +  car3.getInitialPriceprice() + "\n Ann�e de production : "  + car3.getManufacturingYear() + "\n Ref. : " + car3.getRegistrationNumber() + "\n Acqu�reur: " + car3.getUser().name);
		
		car1.calculEstimation(car1.getManufacturingYear(), car1.getBrand(), car1.getInitialPriceprice());
		car2.calculEstimation(car2.getManufacturingYear(), car2.getBrand(), car2.getInitialPriceprice());
		car3.calculEstimation(car3.getManufacturingYear(), car3.getBrand(), car3.getInitialPriceprice());
		car1.calculAssurance(car1.getInitialPriceprice());

}
	

	

	
}
